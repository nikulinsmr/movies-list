export const concatArraysByKey = (array1, array2, key) => {
  const result = [...array1];
  array2.forEach(newElement => {
    const oldIndex = result.findIndex(oldElement => oldElement[key] === newElement[key]);
    if (oldIndex === -1) {
      result.push(newElement);
    } else {
      result[oldIndex] = newElement;
    }
  });
  return result;
};

export const joinArrays = (array1, array2, newKey, foreignKey, relatedKey) => {
  return array1.map(element => {
    element[newKey] = array2.find(
      relatedElement => element[foreignKey] === relatedElement[relatedKey],
    );
    return element;
  });
};
