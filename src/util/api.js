import RequestParams from './apiRequestParams';

export default class Api {
  static BASE_URL = process.env.VUE_APP_API_BASE_URL;

  static buildRequestParams() {
    return new RequestParams();
  }

  static async getMovies(requestParams = Api.buildRequestParams()) {
    const request = new Request(Api.BASE_URL + 'movies?' + requestParams.toUrlString());
    const result = await Api.fetchJson(request);
    if (!Array.isArray(result)) throw 'Invalid server response';
    return result;
  }

  static async getDirectors() {
    const request = new Request(Api.BASE_URL + 'directors');
    const result = await Api.fetchJson(request);
    if (!Array.isArray(result)) throw 'Invalid server response';
    return result;
  }

  static async fetchJson(request) {
    const response = await fetch(request);
    const json = await response.json();
    return json;
  }
}
