export default class RequestParams {
  constructor() {
    this.page = 1;
    this.perPage = 10;
    this.sortField = null;
    this.sortOrder = null;
    this.filters = {};
  }

  setPage(n) {
    this.page = n;
    return this;
  }

  setPerPage(n) {
    this.perPage = n;
    return this;
  }

  sortByField(fieldName, order = 'asc') {
    this.sortField = fieldName;
    this.sortOrder = order;
    return this;
  }

  filterByField(fieldName, query) {
    if (!fieldName || !query) return this;
    this.filters[fieldName] = query;
    return this;
  }

  filterByFields(filters) {
    this.filters = filters;
    return this;
  }

  toUrlString() {
    const searchParams = new URLSearchParams();
    searchParams.append('_page', this.page);
    searchParams.append('_limit', this.perPage);
    Object.keys(this.filters).forEach(field => {
      if (this.filters[field]) {
        searchParams.append(field, this.filters[field]);
      }
    });
    if (this.sortField) {
      searchParams.append('_sort', this.sortField);
      searchParams.append('_order', this.sortOrder);
    }
    return searchParams.toString();
  }
}
